import random
from time import sleep as s
import copy
from sys import argv
from getch import getch

# v for verbose
try:
    v = bool(argv[2])
except:
    v = False


# ------------------- CREATION & SORTING ALGORITHM ------------------- #

# Class for each position on the board
class Locale():
    actual = 0
    num = '\033[31mX\033[0m'
    def __init__(self, spot):
        self.spot = spot
        self.opposite = spot+2*(40-spot)
        self.empty = False

locations = []

# Create a board
for i in range(81):
    locations.append(Locale(i))

board = '''
╔═══════╦═══════╦═══════╗
║ {} {} {} ║ {} {} {} ║ {} {} {} ║
║ {} {} {} ║ {} {} {} ║ {} {} {} ║
║ {} {} {} ║ {} {} {} ║ {} {} {} ║
╠═══════╬═══════╬═══════╣
║ {} {} {} ║ {} {} {} ║ {} {} {} ║
║ {} {} {} ║ {} {} {} ║ {} {} {} ║
║ {} {} {} ║ {} {} {} ║ {} {} {} ║
╠═══════╬═══════╬═══════╣
║ {} {} {} ║ {} {} {} ║ {} {} {} ║
║ {} {} {} ║ {} {} {} ║ {} {} {} ║
║ {} {} {} ║ {} {} {} ║ {} {} {} ║
╚═══════╩═══════╩═══════╝
'''

# Function to show the board whenever we want
# Can clear the screen or just print on a new line
def showBoard(locations,clearScreen):
    locationNumbers = [locations[i].num for i in range(81)]
    if clearScreen:
        print("\033[H\033[2J",board.format(*locationNumbers))
    else:
        print(board.format(*locationNumbers))

# Grab the rows, columns, and boxes of the board
row = lambda board,q: [board[i] for i in range(9*q,9*(q+1))]
col = lambda board,q: [board[i] for i in range(q,81,9)]
box = lambda board,q,r: [board[i] for j in range(3) for i in range(3*(9*q+r%3+3*j),3*(9*q+r%3+3*j+1))]

rows = [row(locations, q) for q in range(9)]
cols = [col(locations, q) for q in range(9)]
boxes = [box(locations, q,r) for q in range(3) for r in range(3)]

# Assign a row, col, and box attribute for each location
for i in range(9):
    for space in rows[i]:
        space.r = i
    for space in cols[i]:
        space.c = i
    for space in boxes[i]:
        space.b = i


# --------------------- COMPLETE GENERATION ALGORITHM --------------------- #

# findImpossible returns all possible number assignments
# for a given spot at row r, column c, and in box b
# Also takes into account saved impossible values from
# trial and error in generation
def findImpossible(board, r,c,b,savedImpossible):
    numbers = [i for i in range(1,10)]
    impossibles = [savedImpossible]
    for space in [row(board, q) for q in range(9)][r]:
        if space.num != '\033[31mX\033[0m':
            impossibles.append(space.num)
    for space in [col(board, q) for q in range(9)][c]:
        if space.num != '\033[31mX\033[0m':
            impossibles.append(space.num)
    for space in [box(board, q,r) for q in range(3) for r in range(3)][b]:
        if space.num != '\033[31mX\033[0m':
            impossibles.append(space.num)

    possibles = [x for x in numbers if x not in impossibles]

    if possibles == []:
        err = "No possibles"
    else:
        err = None

    # Return error in a sense, explains why the generation backtracks
    return possibles, err

# Loop to go location by location assigning numbers
i = 0
totalPlaced = 0
savedImpossible = 0
while i < 81:
    space = locations[i]
    possibles, err = findImpossible(locations, space.r, space.c, space.b, savedImpossible)
    if err == None:
        space.num = random.choice(possibles)
        savedImpossible = 0
        i += 1
    else:
        # Jump back 6 spots
        # Reset the last 6 locations that have been set
        toReset = locations[i-5:i+1]
        for location in toReset:
            location.num = '\033[31mX\033[0m'
        savedImpossible = locations[i-6].num
        i -= 6
    if v:
        showBoard(locations, True)
        print("Number filled:\033[31m "+str(i)+"\033[0m")
        print("Total Placed:\033[31m  "+str(totalPlaced)+"\033[0m")
        s(.05)
    totalPlaced += 1

    # If the generation doesn't look like it is going anywhere
    if totalPlaced > 1500:
        # FIX THIS so it doesn't stop the program, just tries again
        print("Please try again")
        exit()

# Make a copy of the board so we can erase numbers from
# it while retaining the solution
complete = copy.deepcopy(locations)


# ---------------------- PUZZLE SETUP ALGORITHM ---------------------- #

def setup(removeNum, completed):
    toRemove = random.sample([i for i in range(41)],int(numRemove/2))
    for i in toRemove:
        # Remove a location and its opposite, meaning the solution
        # should still be unique, assuming number combination has not
        # been removed before
        space = completed[int(i)]
        space.num = '\033[31mX\033[0m'
        completed[space.opposite].num = '\033[31mX\033[0m'
        space.empty = True
        completed[space.opposite].empty = True
        if v:
            showBoard(completed, True)
            s(.3)

    # Incomplete puzzle board to show, if unique
    incomplete = copy.deepcopy(completed)

    emptied = [space for space in completed if space.empty == True]

    for _ in range(len(emptied)):
        for i in range(len(emptied)):
            space = emptied[i]
            possibles, err = findImpossible(completed, space.r, space.c, space.b, savedImpossible)
            if err == None:
                if len(possibles) == 1:
                    space.num = possibles[0]
                    space.empty = False
                    s(v*.03)
                else:
                    pass
            else:
                pass
            empty = [space for space in completed if space.empty == True]
            if v:
                showBoard(completed, True)

    empty = [space for space in completed if space.empty == True]
    if len(empty) == 0:
        for i in range(81):
            incomplete[i].correct = locations[i].num
        showBoard(incomplete, True)
        showBoard(locations, False)
        print("Generated a board with one unique solution and " + str(removeNum) + " numbers removed")
        game(incomplete, locations)


# ----------------------------- GAMEPLAY ----------------------------- #

def game(missing, completed):
    currentLocationNumber = 0
    while True:
        toShow = copy.deepcopy(missing)

        completeNumberList = [int(completed[i].num) for i in range(81)]
        missingNumberList = [missing[i].actual for i in range(81)]
        if completeNumberList == missingNumberList:
            showBoard(toShow,True)
            print("Congratulations! You completed the Sudoku!")
            exit()
        else:
            toShow[currentLocationNumber].num = '\033[47m'+str(toShow[currentLocationNumber].num)+'\033[0m'
            showBoard(toShow,True)

        for spot in missing:
            if '\033[' in str(spot.num):
                if 'X' in spot.num:
                    spot.actual = 0
                else:
                    spot.actual = int(spot.num[5])
            else:
                spot.actual = spot.num
        currentSpot = missing[currentLocationNumber]
        print("""Please use the arrow keys to move 
and numbers for placement

Press q to quit""")

        userInput = getch()
        if userInput == '\x1b' and getch()=='[':
            # for arrow keys
            userInput = 'm'+getch()
        if userInput == 'mA':
            # UP
            if currentLocationNumber > 8:
                currentLocationNumber -= 9
        elif userInput == 'mB':
            # DOWN
            if currentLocationNumber < 72:
                currentLocationNumber += 9
        elif userInput == 'mC':
            # RIGHT
            if currentLocationNumber%9 != 8:
                currentLocationNumber += 1
        elif userInput == 'mD':
            # LEFT
            if currentLocationNumber%9 != 0:
                currentLocationNumber -= 1 
        elif userInput in [str(i) for i in list(range(1,10))]:
            # Number input
            if '\033[' in str(currentSpot.num):
                currentSpot.num = '\033[34m'+userInput+'\033[0m'
                currentSpot.actual = int(userInput)
        elif userInput in ['x','0',' ']:
            if '\033[' in str(currentSpot.num):
                currentSpot.num = '\033[31mX\033[0m'
                currentSpot.actual = 0
        elif userInput == 'q':
            exit()


# ----------------------------- RUNTIME -------------------------------- #

try:
    numRemove = int(argv[1])
    while True:
        setup(numRemove, copy.deepcopy(complete))
except IndexError:
    print("ERROR: `python3 doku.py [numRemove] [verbose (optional)]`\n - numRemove not valid") 
